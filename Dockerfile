FROM python:3.9-slim-buster
EXPOSE 5000
RUN pip install pipenv
RUN mkdir /app
WORKDIR /app
COPY app/. /app
COPY Pipfile /app
COPY Pipfile.lock /app
RUN pipenv install --deploy --system --ignore-pipfile
ENTRYPOINT [ "gunicorn", "--bind", "0.0.0.0:5000", "wsgi:app" ]

# bsapi21

Simple API for BASIC SKILLS 2021 | WEB task written on python/flask and deployed using Docker.

## API description

Available on [Swaggerhub](https://swaggerhub.com/).

### Versions

- [ ] [API v1.0.0](https://app.swaggerhub.com/apis-docs/ivbignal/BS2021/1.0.0)
- [ ] [API v1.1.0](https://app.swaggerhub.com/apis-docs/ivbignal/BS2021/1.1.0)

## Problems

Problems descriptions available on [SkillFab Cloud](https://cloud.skillfab.pro/s/ywfwj9MlSg6CEE4).

## Answers

Answers will be published on [SkillFab Cloud](https://cloud.skillfab.pro/s/ywfwj9MlSg6CEE4?path=%2FAnswers)

Answer [upload link](https://cloud.skillfab.pro/s/5eRcOfxASFqniAC).
from flask import Flask, jsonify
import json

app = Flask(__name__)

@app.route('/api/v1.0.0/user')
@app.route('/api/v1.1.0/user')
def api_get_user():
    user = {
        "username": "user10302583",
        "last_name": "Иванов",
        "name": "Иван",
        "patronymic": "Иванович",
        "profile_picture": "https://cdn2.thecatapi.com/images/KXVo5br4c.jpg"
    }
    return user

@app.route('/api/v1.0.0/friends')
@app.route('/api/v1.1.0/friends')
def api_get_friends():
    with open('persons.json') as f:
        friends = json.load(f)
    return jsonify(friends)

@app.route('/api/v1.1.0/courses')
def api_get_courses():
    with open('courses.json') as f:
        friends = json.load(f)
    return jsonify(friends)